function invalidTokenException(e) {
    $.ajax({
        type : "POST",
        contentType: 'application/json; charset=utf-8',
        url : "./exception/invalid-token",
        success : function() {
        	location.reload();
            console.log("SUCCESS");
        },
        error : function(e) {
            console.log("ERROR: ", e);
        },
        done : function(e) {
            console.log("DONE");
        }
    });
}

function getUser() {
	$.ajax({
        type : "GET",
        contentType: 'application/json; charset=utf-8',
        url : "./common/user",
        dataType : 'json',
        success : function(data) {
        	$('#remoteUser').append(data.remoteUser);
        	$('#adminType').append(data.adminType);
            console.log("SUCCESS: ", data);
        },
        error : function(e) {
            console.log("ERROR: ", e);
        },
        done : function(e) {
            console.log("DONE");
        }
    });
}

function getAdminData() {
    $.ajax({
        type : "GET",
        contentType: 'application/json; charset=utf-8',
        url : "./admin/all",
        dataType : 'json',
        success : function(data) {
        	$('#adminData').append(data);
            console.log("SUCCESS: ", data);
        },
        error : function(e) {
            console.log("ERROR: ", e);
            invalidTokenException(e);
        },
        done : function(e) {
            console.log("DONE");
        }
    });
}

function getBoardData() {
    $.ajax({
        type : "GET",
        contentType: 'application/json; charset=utf-8',
        url : "./board/all",
        dataType : 'json',
        success : function(data) {
        	$('#boardData').append(data);
            console.log("SUCCESS: ", data);
        },
        error : function(e) {
            console.log("ERROR: ", e);
            invalidTokenException(e);
        },
        done : function(e) {
            console.log("DONE");
        }
    });
}

$( document ).ready(function() {
	getUser();
	getAdminData();
	getBoardData();
});