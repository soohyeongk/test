function getUrls() {
	$.ajax({
        type : "GET",
        contentType: 'application/json; charset=utf-8',
        url : "./common/urls",
        dataType : 'json',
        success : function(data) {
        	$('#adminUrl').attr("href", data.adminUrl);
        	$('#boardUrl').attr("href", data.boardUrl);
            console.log("SUCCESS: ", data);
        },
        error : function(e) {
            console.log("ERROR: ", e);
        },
        done : function(e) {
            console.log("DONE");
        }
    });
}

function getRedisSessionInfo() {
	$.ajax({
        type : "GET",
        contentType: 'application/json; charset=utf-8',
        url : "./common/redis-session-info",
        dataType : 'json',
        success : function(data) {
        	console.log(data);
        	var row = "";
        	for(var key in data) {
        		row += "<span>"+key+"</span> " +
        			   "<button type='button' onclick='deleteRedisSessionWithKey(\""+key+"\")'>delete</button><br>" +
        			   "<div>"+JSON.stringify(data[key])+"</div><br>"
        	}
        	$('#keys').append(row);
            console.log("SUCCESS: ", data);
        },
        error : function(e) {
        	console.log("ERROR: ", e);
        },
        done : function(e) {
            console.log("DONE");
        }
    });
}

function deleteRedisSessionWithKey(key) {
	$.ajax({
        type : "POST",
        contentType: 'application/json; charset=utf-8',
        url : "./common/delete-session-key",
        data : key,
        success : function() {
            console.log("SUCCESS");
            location.reload();
        },
        error : function(e) {
        	console.log("ERROR: ", e);
        },
        done : function(e) {
            console.log("DONE");
        }
    });
}

$( document ).ready(function() {
	getUrls();
	getRedisSessionInfo();
});