package com.northstar.front.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ViewController {
    @GetMapping("favicon.ico")
    @ResponseBody 
    void returnNoFavicon() {
    }
    @RequestMapping(value = "/{url}")
    public String mainView(@PathVariable("url") String url, HttpServletRequest request, Model model) throws Exception {
        return url;
    }
}
