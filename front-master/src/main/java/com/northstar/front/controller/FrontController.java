package com.northstar.front.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.northstar.common.utils.Utils;
import com.northstar.common.vo.OAuthToken;

@RestController
public class FrontController {
	@Autowired
    RedisTemplate<String, Object> redisTemplate;
	
	@Value("${oauth.host}")
	private String oauthHost;
	@Value("${oauth.port}")
	private String oauthPort;
	@Value("${admin.host}")
	private String adminHost;
	@Value("${admin.port}")
	private String adminPort;
	@Value("${board.host}")
	private String boardHost;
	@Value("${board.port}")
	private String boardPort;
	@Value("${apicast.host}")
	private String apicastHost;
	@Value("${apicast.user.key}")
	private String apicastUserKey;
	
	@RequestMapping(value = "/common/urls", method = RequestMethod.GET)
	public HashMap<String, String> getUrls() {
		HashMap<String, String> rtnMap = new HashMap<>();
        rtnMap.put("adminUrl", "http://"+Utils.generateFqdn(adminHost, adminPort));
        rtnMap.put("boardUrl", "http://"+Utils.generateFqdn(boardHost, boardPort));
        
        return rtnMap;
	}
	
	@RequestMapping(value = "/common/user", method = RequestMethod.GET)
	public HashMap<String, String> getUser() {
		HashMap<String, String> rtnMap = new HashMap<>();
		ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        OAuthToken oAuthToken = (OAuthToken)valueOperations.get("frontToken");
        rtnMap.put("remoteUser", oAuthToken.getRemote_user());
        rtnMap.put("adminType", oAuthToken.getAdmin_type());
        
        return rtnMap;
	}
	
	@RequestMapping(value = "/common/redis-session-info", method = RequestMethod.GET)
	public HashMap<String, Object> getRedisSessionInfo() {
		HashMap<String, Object> rtnMap = new HashMap<>();
		ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
		for(String key : redisTemplate.keys("*Token")) {
			rtnMap.put(key, valueOperations.get(key));
		}
        return rtnMap;
	}
	
	@RequestMapping(value = "/common/delete-session-key", method = RequestMethod.POST)
	public void deleteSessionKey(@RequestBody String key) {
		redisTemplate.delete(key);
	}
	
	@RequestMapping(value = "/admin/all", method = RequestMethod.GET)
	public String getAdminData(HttpServletRequest request) {
		String url = null;
		if(!Utils.isEmpty(apicastHost)) {
			url = "https://"+apicastHost+"/admin/users"+"?user_key="+apicastUserKey;
		} else {
			url =  "http://"+Utils.generateFqdn(adminHost, adminPort)+"/api/admin/users";
		}
		return Utils.exchange(request, url, redisTemplate);
	}
	
	@RequestMapping(value = "/board/all", method = RequestMethod.GET)
	public String getBoardData(HttpServletRequest request) {
		String url = null;
		if(!Utils.isEmpty(apicastHost)) {
			url = "https://"+apicastHost+"/board/content"+"?user_key="+apicastUserKey;
		} else {
			url =  "http://"+Utils.generateFqdn(boardHost, boardPort)+"/api/board/content";
		}
		return Utils.exchange(request, url, redisTemplate);
	}
	
	@RequestMapping(value = "/user/logout", method = RequestMethod.GET)
    public void logout(HttpServletRequest request, HttpServletResponse response) throws Exception {
		for(String key : redisTemplate.keys("*Token")) {
    		redisTemplate.delete(key);
    	}
        response.sendRedirect("http://"+Utils.generateFqdn(oauthHost, oauthPort)+"/oauth/logout");
    }
}
