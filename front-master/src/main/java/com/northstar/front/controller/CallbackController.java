package com.northstar.front.controller;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.northstar.common.utils.Utils;
import com.northstar.common.vo.OAuthToken;

@RestController
@RequestMapping("/oauth")
public class CallbackController {
	@Autowired
    RedisTemplate<String, Object> redisTemplate;
	
	@Value("${local.host}")
	private String localHost;
	@Value("${server.port}")
	private String localPort;
	@Value("${oauth.host}")
	private String oauthHost;
	@Value("${oauth.port}")
	private String oauthPort;
	@Value("${security.oauth2.client.client-id}")
	private String clientId;
	@Value("${security.oauth2.client.client-secret}")
	private String clientSecret;
	
	private final Gson gson = new Gson();
    private final RestTemplate restTemplate = new RestTemplate();
    
	@RequestMapping(value = "/authorization-code")
	public ModelAndView callbackSocial(@RequestParam String code, HttpSession session) throws RestClientException, URISyntaxException {	
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(clientId, clientSecret);
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("code", code);
        params.add("grant_type", "authorization_code");
        params.add("scope", "read");
        params.add("client_id", clientId);
        params.add("client_secret", clientSecret);
        params.add("redirect_uri", "http://"+Utils.generateFqdn(localHost, localPort)+"/oauth/authorization-code");
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(params, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(new URI("http://"+Utils.generateFqdn(oauthHost, oauthPort)+"/oauth/token"), request, String.class);
        if (response.getStatusCode() == HttpStatus.OK) {
        	OAuthToken token = gson.fromJson(response.getBody(), OAuthToken.class);
        	ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        	valueOperations.set("frontToken", token);
        	return new ModelAndView("redirect:/");
        }
        return null;
	}
}
