package com.northstar.front.controller;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.northstar.common.utils.Utils;
import com.northstar.common.vo.OAuthToken;

@RestController
@RequestMapping("/exception")
public class ExceptionController {
	@Autowired
    RedisTemplate<String, Object> redisTemplate;
	
	@Value("${local.host}")
	private String localHost;
	@Value("${server.port}")	
	private String localPort;
	@Value("${oauth.host}")
	private String oauthHost;
	@Value("${oauth.port}")
	private String oauthPort;
	@Value("${security.oauth2.client.client-id}")
	private String clientId;
	@Value("${security.oauth2.client.client-secret}")
	private String clientSecret;
	
	private final Gson gson = new Gson();
    private final RestTemplate restTemplate = new RestTemplate();
	
	@RequestMapping(value = "/invalid-token", method = RequestMethod.POST)
	public void getReAccessToken() throws RestClientException, URISyntaxException {
		ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
		OAuthToken previousToken = (OAuthToken)valueOperations.get("frontToken");
		
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(clientId, clientSecret);
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("grant_type", "refresh_token");
        params.add("refresh_token", previousToken.getRefresh_token());
        params.add("client_id", clientId);
        params.add("client_secret", clientSecret);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(params, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(new URI("http://"+Utils.generateFqdn(oauthHost, oauthPort)+"/oauth/token"), request, String.class);
        if (response.getStatusCode() == HttpStatus.OK) {
        	OAuthToken token = gson.fromJson(response.getBody(), OAuthToken.class);
        	valueOperations.set("frontToken", token);
        }
	}
}
