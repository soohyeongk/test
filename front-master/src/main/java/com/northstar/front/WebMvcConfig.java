package com.northstar.front;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.northstar.front.interceptor.AuthInterceptor;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
	@Autowired
    RedisTemplate<String, Object> redisTemplate;
	
	@Value("${local.host}")
	private String localHost;
	@Value("${server.port}")
	private String localPort;
	@Value("${oauth.host}")
	private String oauthHost;
	@Value("${oauth.port}")
	private String oauthPort;
	@Value("${security.oauth2.client.client-id}")
	private String clientId;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
    	List<String> pattenList = new ArrayList<>();
        pattenList.add("/lib/js/**");
        pattenList.add("/lib/css/**");
        pattenList.add("/js/**");
        pattenList.add("/css/**");
        pattenList.add("/public/**");
        pattenList.add("/static/**");
        pattenList.add("/oauth/**");
        pattenList.add("/error");
        pattenList.add("/");
        pattenList.add("/index");
        pattenList.add("/common/**");
        pattenList.add("/exception/**");
        registry.addInterceptor(new AuthInterceptor(localHost, localPort, oauthHost, oauthPort, clientId, redisTemplate))
                .addPathPatterns("/**")
                .excludePathPatterns(pattenList);
    }
    
    @Override
    public void addViewControllers(ViewControllerRegistry registry ) {
    	registry.addViewController("/").setViewName("forward:/index");
    }

}
