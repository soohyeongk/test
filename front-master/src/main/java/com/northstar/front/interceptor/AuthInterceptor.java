package com.northstar.front.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.northstar.common.utils.Utils;
import com.northstar.common.vo.OAuthToken;

public class AuthInterceptor extends HandlerInterceptorAdapter {
	private String localHost;
	private String localPort;
	private String oauthHost;
	private String oauthPort;
	private String clientId;
	private RedisTemplate<String, Object> redisTemplate;
	
	public AuthInterceptor(String localHost, String localPort, String oauthHost, String oauthPort, String clientId, RedisTemplate<String, Object> redisTemplate) {
		this.localHost = localHost;
		this.localPort = localPort;
		this.oauthHost = oauthHost;
		this.oauthPort = oauthPort;
		this.clientId = clientId;
		this.redisTemplate = redisTemplate;
	}
	
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    	ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        OAuthToken oAuthToken = (OAuthToken)valueOperations.get("frontToken");
        
        if(oAuthToken == null && !request.getRequestURI().equals("/favicon.ico")) {
        	response.sendRedirect("http://"+Utils.generateFqdn(oauthHost, oauthPort)+"/oauth/authorize?response_type=code&client_id="+clientId+"&redirect_uri=http://"+Utils.generateFqdn(localHost, localPort)+"/oauth/authorization-code&scope=read");
            return true;
        } else {
            return true;
        }
    }
}
