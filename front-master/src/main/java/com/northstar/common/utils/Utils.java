package com.northstar.common.utils;

import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.northstar.common.vo.OAuthToken;

public class Utils {
	public static boolean isEmpty(Object obj) {
		if (obj == null) return true;
		if ((obj instanceof String) && (((String)obj).trim().length() == 0)) { return true; }
		if (obj instanceof Map) { return ((Map<?, ?>) obj).isEmpty(); }
		if (obj instanceof Map) { return ((Map<?, ?>)obj).isEmpty(); } 
		if (obj instanceof List) { return ((List<?>)obj).isEmpty(); }
		if (obj instanceof Object[]) { return (((Object[])obj).length == 0); }

		return false;
	}
	
	public static String generateFqdn(String host, String port) {
		String fqdn;
		
    	if(Utils.isEmpty(port)) {
    		fqdn = host;
    	} else {
    		fqdn = host+":"+port;
    	}
    	return fqdn;
	}
	
	public static String exchange(HttpServletRequest request, String url, RedisTemplate<String, Object> redisTemplate) {
		String rtnVal = "";
		
		try {
			// SSL 인증 관련
			TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

			SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
			        .loadTrustMaterial(null, acceptingTrustStrategy)
			        .build();

			SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

			CloseableHttpClient httpClient = HttpClients.custom()
			        .setSSLSocketFactory(csf)
			        .build();

			HttpComponentsClientHttpRequestFactory factory =
			        new HttpComponentsClientHttpRequestFactory();

			factory.setHttpClient(httpClient);
//			HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
			RestTemplate restTemplate = new RestTemplate(factory);
			
			ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
	        OAuthToken oAuthToken = (OAuthToken)valueOperations.get("frontToken");
	        
	        String token = "";
	        
	        if(oAuthToken != null) {
	        	token = oAuthToken.getAuthorization();
	        }
	        
	        HttpHeaders header = new HttpHeaders();
        	
	        header.add("Authorization", token);
	        header.add("isExternalRequest", "TRUE");
			
			HttpEntity<?> entity = new HttpEntity<>(header);
			
			UriComponents uri = UriComponentsBuilder.fromHttpUrl(url).build();
			
			ResponseEntity<String> resultMap = restTemplate.exchange(uri.toString(), HttpMethod.GET, entity, String.class);
			
	        ObjectMapper mapper = new ObjectMapper();
	        rtnVal = mapper.writeValueAsString(resultMap.getBody());
	        
        } catch (Exception e) {
        	e.printStackTrace();
        }
		
		return rtnVal;
	}
	
//	public static String httpsExchange(HttpServletRequest request, String url, String path, String query, RedisTemplate<String, Object> redisTemplate) {
//		String rtnVal = "";
//		
//		try {
//			HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
//			RestTemplate restTemplate = new RestTemplate(factory);
//			
//			ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
//	        OAuthToken oAuthToken = (OAuthToken)valueOperations.get("frontToken");
//	        
//	        String token = "";
//	        
//	        if(oAuthToken != null) {
//	        	token = oAuthToken.getAuthorization();
//	        }
//	        
//	        HttpHeaders header = new HttpHeaders();
//        	
//	        header.add("Authorization", token);
//	        header.add("isExternalRequest", "TRUE");
//			
//			HttpEntity<?> entity = new HttpEntity<>(header);
//			
//			UriComponents uri = UriComponentsBuilder.newInstance().scheme("https").host(url).path(path).query(query).build();
//			System.out.println(uri);
//			
//			ResponseEntity<String> resultMap = restTemplate.exchange(uri.toString(), HttpMethod.GET, entity, String.class);
//			
//	        ObjectMapper mapper = new ObjectMapper();
//	        rtnVal = mapper.writeValueAsString(resultMap.getBody());
//	        System.out.println(rtnVal);
//	        
//        } catch (Exception e) {
//        	e.printStackTrace();
//        	System.out.println(e.toString());
//        }
//		
//		return rtnVal;
//	}
}
